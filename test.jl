using MAT # read Matlab matrix files
using IterTools # function subset (nchoosek)
using LinearAlgebra
using Statistics
using Plots
Plots.plotlyjs()

using GIANT


##############################################################################
# Auxiliary functions

function checkNNIndepUntilOrder4(W, k)
    r = size(W, 2)

    combis = hcat(collect(subsets(1:r, 2))...)'
    WoW = W[:,combis[:,1]] .* W[:,combis[:,2]]
    Wtot = hcat(W,WoW)

    if r >= 3
        combis = hcat(collect(subsets(1:r, 3))...)'
        WoWoW = W[:,combis[:,1]] .* W[:,combis[:,2]] .* W[:,combis[:,3]]
        Wtot = hcat(Wtot, WoWoW)
    end

    if r >= 4
        combis = hcat(collect(subsets(1:r, 4))...)'
        WoWoWoW = W[:,combis[:,1]] .* W[:,combis[:,2]] .* W[:,combis[:,3]] .* W[:,combis[:,4]]
        Wtot = hcat(Wtot,WoWoWoW)
    end

    # display(Wtot)

    alpha, idxmin = alphaparam_delta(Wtot, k)
    return alpha
end


# Given an m-by-r matrix W, compute:
# alpha = min_i min_{x >= 0} ||W[:,i] - W[:,I] x||_2 / ||W(:,i)||_2
# where I = {1,2,...,r}\{i}
# and idxmin = index for which the minimum is achieved.
function alphaparam_delta(W, k)
    r = size(W, 2)

    alpha = +Inf
    idxmin = 0

    for i in 1:r
        if alpha > 1e-6
            A = W[:,1:end .!= i]
            b = W[:,i]
            AtA = A'*A
            Atb = A'*b
            x = Vector{Int}()
            if k == 0
                x = activeset(AtA, Atb, sumtoone=true)
            else
                # x, _ = arborescent(AtA, Atb, k, sumtoone=true)
                x, _ = bruteforce(AtA, Atb, k, sumtoone=true)
            end
            alphai = norm(A*x - b)
            if alphai < alpha
                alpha = alphai;
                idxmin = i;
            end
        end
    end

    return alpha, idxmin
end


##############################################################################
# Test functions

# Test with spectral data
function dotestspectral()
    nbmc = 100 # number of monte carlo per parameter

    # ranks = [2,3,4,6,8,10]
    ranks = [2,3,4,6,8]
    nbranks = length(ranks)
    sparsities = [1,2]

    results = ones(nbmc,nbranks,length(sparsities))
    for (idxr, r) in enumerate(ranks)
        Threads.@threads for idxmc in 1:nbmc
            for (idxk, k) in enumerate(sparsities)
                datafile = "data-christophe/data/data_indNbSpectres$(idxr)_it_MC$(idxmc).mat"
                println("r=$(r) mc=$(idxmc) k=$(k)")
                data = matopen(datafile)
                W0 = Array(read(data, "W0"))
                alpha = checkNNIndepUntilOrder4(W0[:,1:r], k)
                results[idxmc, idxr, idxk] = alpha
            end
        end
    end


    alphatab = Array(read(matopen("data-christophe/Results.mat"), "alphaTab"))

    display(mean(alphatab[:,1:nbranks], dims=1)')
    p = plot(ranks, mean(alphatab[:,1:nbranks], dims=1)', xlabel="Rank r", ylabel="alpha_4", label="Christophe")
    for (idxk, k) in enumerate(sparsities)
        println("k=$(k)")
        display(mean(results[:,1:nbranks,k], dims=1)')
        plot!(p, ranks, mean(results[:,1:nbranks,idxk], dims=1)', label="k=$(k)")
    end
    gui()
end


# Test with random data
function dotestrand()
    nbmc = 30 # number of monte carlo per parameter
    mvalues = [20,50,100,150,200] # number of rows in W0

    # ranks = [2,3,4,6,8,10]
    ranks = [2,3,4,6,8]
    nbranks = length(ranks)
    sparsities = [1,2]

    for m in mvalues
        alphatab = ones(nbmc, nbranks)
        results = ones(nbmc,nbranks,length(sparsities))
        for (idxr, r) in enumerate(ranks)
            Threads.@threads for idxmc in 1:nbmc
                println("m=$(m) r=$(r) mc=$(idxmc)")
                W0 = rand(m, r)
                alphatab[idxmc, idxr] = checkNNIndepUntilOrder4(W0, 0)
                for (idxk, k) in enumerate(sparsities)
                    println("k=$(k)")
                    alpha = checkNNIndepUntilOrder4(W0, k)
                    results[idxmc, idxr, idxk] = alpha
                end
            end
        end

        display(mean(alphatab[:,1:nbranks], dims=1)')
        p = plot(ranks, mean(alphatab[:,1:nbranks], dims=1)', xlabel="Rank r", ylabel="alpha_4", label="k=r")
        for (idxk, k) in enumerate(sparsities)
            println("k=$(k)")
            display(mean(results[:,1:nbranks,k], dims=1)')
            plot!(p, ranks, mean(results[:,1:nbranks,idxk], dims=1)', label="k=$(k)")
        end
        savefig(p, "results-m$(m).html")
    end
end


# dotestspectral()
dotestrand()
